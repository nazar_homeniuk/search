﻿using Microsoft.AspNetCore.Mvc;
using Search.API.Services;
using Search.Core.Models;

namespace Search.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService productService;

        public ProductController(IProductService productService)
        {
            this.productService = productService;
        }

        [Route("create")]
        [HttpPost]
        public IActionResult Create(ProductDto productDto)
        {
            productService.Create(productDto);
            return Ok();
        }

        [Route("update")]
        [HttpPost]
        public IActionResult Update(ProductDto productDto)
        {
            productService.Update(productDto);
            return Ok();
        }
    }
}