﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Search.BLL.Interfaces;
using Search.BLL.Models;
using Search.BLL.Services;
using Search.Core.Models;

namespace Search.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SearchController : ControllerBase
    {
        private readonly ISearchService<ProductDto> searchService;

        public SearchController()
        {
            searchService = new SearchService();
        }

        [HttpPost]
        public IEnumerable<ProductDto> Search(SearchRequest request)
        {
            return searchService.Search(request);
        }

        [Route("autocomplete/{query}")]
        [HttpGet]
        public IEnumerable<string> Autocomplete(string query)
        {
            return searchService.Autocomplete(query);
        }
    }
}
