﻿using System.Threading.Tasks;
using NServiceBus;
using Search.Core.Commands;
using Search.Core.Models;


namespace Search.API.Services
{
    public interface IProductService
    {
        Task Create(ProductDto product);
        Task Update(ProductDto product);
    }

    public class ProductService : IProductService
    {
        private readonly IMessageSession messageSession;
        public ProductService(IMessageSession messageSession)
        {
            this.messageSession = messageSession;
        }

        public async Task Create(ProductDto product)
        {
            var command = new CreateCommand(product);
            await messageSession.Send(command).ConfigureAwait(false);
        }

        public Task Update(ProductDto product)
        {
            throw new System.NotImplementedException();
        }
    }
}
