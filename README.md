# XML Search Engine based on Elasticsearch
The engine allows you to index and search data in XML files. The application consists of three parts: Elasticsearch server, back-end (ASP.NET Core Web API 2) and front-end (Angular 7.2.3).
## Setup

### RabbitMQ installation guide
RabbitMQ requires a 64-bit [supported version of Erlang](https://www.rabbitmq.com/which-erlang.html) for Windows to be installed. There's a [Windows installer for Erlang](http://www.erlang.org/downloads). **Important**: the Erlang installer *must* be run using an administrative account otherwise a registry key expected by the RabbitMQ installer will not be present.

- [Download](https://www.rabbitmq.com/download.html) and install the RabbitMQ server.
- The RabbitMQ service starts automatically. You can stop/reinstall/start the RabbitMQ service from the Start Menu.
### Elasticsearch installation guide
- First of all, you need to [download](https://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-5066655.html) and install JDK. After this, restart your computer.
- [Download](https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-6.6.0.zip) and unzip Elasticsearch.
- Open Powershell, then go to the Elasticsearch folder and run: 
```sh
$ bin\elasticsearch.bat
```
- After this, run:
```sh
$ Invoke-RestMethod http://localhost:9200
```
- Dive into the [getting started guide](https://www.elastic.co/guide/en/elasticsearch/reference/current/getting-started.html) and [video](https://www.elastic.co/webinars/getting-started-elasticsearch).
### Angular project setup
Angular requires Node.js version 8.x or 10.x.

 - To check your version, run node -v in a terminal/console window.
 - To get Node.js, go to nodejs.org.

To run the site:

 - Open the **SearchAngular** folder in Visual Studio Code.
 - Open the terminal.
 - Install the Angular CLI.
```sh
$ npm install -g @angular/cli
```
 - Install all needed packages.
```sh
$ npm install
```
 - Start the server.
```sh
$ ng serve
```
 - Open web browser and go to http://localhost:4200.
### Trying the main flow
To try the main flow you must:

 - start Elasticsearch server;
 - start Web Api server;
 - start Node.js server (Angular project);
 - add XML files with products data to /data ;
 - make GET request to https://localhost:44374/api/search/index ;
 - open web browser and go to http://localhost:4200 ;
 - enter the search request.
### XML example
```xml
<?xml version="1.0"?>
<ArrayOfProduct xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <Product>
    <Id>0</Id>
    <Title>Apple IPhone XS Max</Title>
    <Category>Smartphones</Category>
    <ShopName>Jabko</ShopName>
    <ShopAddress>Shevchenka street, 15</ShopAddress>
    <Price>1350</Price>
    <Count>15</Count>
    <Tags>
      <string>IPhone</string>
      <string>Smartphone</string>
      <string>Apple</string>
      <string>Jabko</string>
      <string>Kyiv</string>
    </Tags>
  </Product>
  <Product>
    <Id>1</Id>
    <Title>Apple MacBook Pro 17</Title>
    <Category>Laptops</Category>
    <ShopName>Jabko</ShopName>
    <ShopAddress>Antonovucha street, 25</ShopAddress>
    <Price>2400</Price>
    <Count>7</Count>
    <Tags>
      <string>PC</string>
      <string>Laptop</string>
      <string>Apple</string>
      <string>MacBook</string>
      <string>Pro</string>
      <string>Jabko</string>
      <string>Kyiv</string>
    </Tags>
  </Product>
</ArrayOfProduct>
```
## Architecture (soon)
...
## Build with
 - [ASP.NET Core Web Api 2](https://docs.microsoft.com/ru-ru/aspnet/core/web-api/?view=aspnetcore-2.2) - The web API framework used.
 - [Angular](https://angular.io/) - Fornt-end framework.
 - [Angular Material](https://material.angular.io/) - Material Design components for Angular.
 - [NEST](https://www.elastic.co/guide/en/elasticsearch/client/net-api/current/nest.html) - Hight level Elasticsearch .NET client.
* [Elasticsearch](https://www.elastic.co/products/elasticsearch) - Search and analytics engine.
* [NServiceBus](https://particular.net/nservicebus) - Service bus for .NET.
* [RabbitMQ](https://www.rabbitmq.com/) - Open source message broker.