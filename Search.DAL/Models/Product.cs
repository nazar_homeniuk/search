﻿using System;
using System.Xml.Serialization;
using Nest;

namespace Search.DAL.Models
{
    [ElasticsearchType(IdProperty = "Id", Name = "product")]
    [Serializable]
    public class Product
    {
        [Text(Name = "id")]
        public string Id { get; set; }
        [Text(Name = "title")]
        public string Title { get; set; }
        [Text(Name = "category")]
        public string Category { get; set; }
        [Text(Name = "shop_name")]
        public string ShopName { get; set; }
        [Text(Name = "shop_address")]
        public string ShopAddress { get; set; }
        [Text(Name= "shop_city")]
        public string ShopCity { get; set; }
        [Number]
        public decimal Price { get; set; }
        [Number]
        public int Count { get; set; }
        public string[] Tags { get; set; }
        [XmlIgnore]
        public CompletionField Suggest { get; set; }
    }
}
