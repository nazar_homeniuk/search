﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Nest;
using Search.DAL.Interfaces;
using Search.DAL.Models;

namespace Search.DAL.Providers
{
    public class XmlDataProvider : IXmlDataProvider<Product>
    {
        private readonly XmlSerializer xmlSerializer;

        public XmlDataProvider()
        {
            xmlSerializer = new XmlSerializer(typeof(List<Product>));
        }

        public IEnumerable<Product> ReadFromDirectory(string path)
        {
            var products = new List<Product>();
            var directory = new DirectoryInfo(path);
            foreach (var directoryInfo in directory.GetDirectories())
            {
                products.AddRange(ReadFromDirectory(directoryInfo.FullName));
            }

            foreach (var fileInfo in directory.GetFiles())
            {
                products.AddRange(ReadFromFile(fileInfo.FullName));
            }

            return products;
        }

        public void Create(Product product, string folderPath)
        {
            var productFilePath = GetProductFilePath(product, folderPath);
            var products = new List<Product>();
            if (productFilePath.Length == 0)
            {
                productFilePath = $"{folderPath}{product.ShopCity}/{product.Category}.xml";
                Directory.CreateDirectory(Path.GetDirectoryName(productFilePath));
                products.Add(product);
                Serialize(products, productFilePath);
                return;
            }

            products = ReadFromFile(productFilePath).ToList();
            products.Add(product);
            Serialize(products, productFilePath);
        }

        public void Update(Product product, string folderPath)
        {
            var productFilePath = GetProductFilePath(product, folderPath);
            if (productFilePath.Length == 0)
            {
                return;
            }

            var products = ReadFromFile(productFilePath).ToList();
            products.RemoveAll(p => p.Id == product.Id);
            products.Add(product);
            Serialize(products, productFilePath);
        }

        public IEnumerable<Product> ReadFromFile(string path)
        {
            List<Product> products;
            using (var fileStream = new FileStream(path, FileMode.OpenOrCreate))
            {
                products = Deserialize(fileStream).ToList();
            }

            InitializeSuggest(products);
            return products;
        }

        public string GetProductFilePath(Product product, string folderPath)
        {
            var directory = new DirectoryInfo(folderPath);
            var result = new StringBuilder();
            foreach (var directoryInfo in directory.GetDirectories())
            {
                result.Append(GetProductFilePath(product, directoryInfo.FullName));
            }

            foreach (var fileInfo in directory.GetFiles())
            {
                if (ReadFromFile(fileInfo.FullName)
                    .Any(p => p.ShopCity == product.ShopCity && p.Category == product.Category))
                {
                    return fileInfo.FullName;
                }
            }

            return result.ToString();
        }

        private void Serialize(IEnumerable<Product> products, string path)
        {
            using (var fileStream = new FileStream(path, FileMode.OpenOrCreate))
            {
                xmlSerializer.Serialize(fileStream, products.ToList());
            }
        }

        private IEnumerable<Product> Deserialize(Stream stream)
        {
            return (IEnumerable<Product>)xmlSerializer.Deserialize(stream);
        }

        private static void InitializeSuggest(IEnumerable<Product> products)
        {
            foreach (var product in products)
            {
                product.Suggest = new CompletionField { Input = (string[])product.Tags.Clone() };
            }
        }
    }
}
