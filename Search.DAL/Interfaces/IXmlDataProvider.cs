﻿using System.Collections.Generic;

namespace Search.DAL.Interfaces
{
    public interface IXmlDataProvider<T>
    {
        IEnumerable<T> ReadFromDirectory(string path);

        IEnumerable<T> ReadFromFile(string path);

        string GetProductFilePath(T instance, string folderPath);

        void Create(T instance, string filePath);

        void Update(T instance, string filePath);
    }
}