﻿using System;

namespace Search.DAL.Config
{
    public class DataProviderConfig
    {
        public static string DataFolderPath = $"{AppDomain.CurrentDomain.BaseDirectory}/data/";
    }
}
