﻿using System;
using Nest;

namespace Search.DAL.Config
{
    public class ElasticConfig
    {
        public static string IndexName => "test";

        public static string ElastisearchUrl => "http://localhost:9200";

        public static IElasticClient GetClient()
        {
            var node = new Uri(ElastisearchUrl);
            var settings = new ConnectionSettings(node).DefaultIndex(IndexName);
            settings.DefaultIndex(IndexName);
            return new ElasticClient(settings);
        }
    }
}
