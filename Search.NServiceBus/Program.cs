﻿using System;
using System.Threading.Tasks;
using Ninject;
using NServiceBus;
using Search.BLL.Services;

namespace Search.NServiceBus
{
    internal class Program
    {
        private static async Task Main()
        {
            var endpointConfiguration = new EndpointConfiguration("NServiceBus");
            var kernel = new StandardKernel();
            kernel.Bind<IndexService>().ToConstant(new IndexService());
            endpointConfiguration.UseContainer<NinjectBuilder>(
                customuzations =>
                {
                    customuzations.ExistingKernel(kernel);
                });
            var transport = endpointConfiguration.UseTransport<RabbitMQTransport>();
            transport.UseConventionalRoutingTopology();
            transport.ConnectionString("host=localhost");
            endpointConfiguration.EnableInstallers();
            endpointConfiguration.UsePersistence<InMemoryPersistence>();
            endpointConfiguration.UseSerialization<NewtonsoftSerializer>();

            var endpointInstance = await Endpoint.Start(endpointConfiguration).ConfigureAwait(false);
            Console.WriteLine("Press any key to stop");
            Console.ReadKey(false);
            await endpointInstance.Stop().ConfigureAwait(false);
        }
    }
}
