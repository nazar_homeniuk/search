﻿using System.Threading.Tasks;
using NServiceBus;
using Search.BLL.Services;
using Search.Core.Commands;

namespace Search.NServiceBus.Handlers
{
    public class CreateCommandHandler : IHandleMessages<CreateCommand>
    {
        private readonly IndexService indexService;

        public CreateCommandHandler(IndexService indexService)
        {
            this.indexService = indexService;
        }

        public async Task Handle(CreateCommand message, IMessageHandlerContext context)
        {
            await indexService.AddProduct(message.Product);
        }
    }
}