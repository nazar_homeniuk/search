﻿using System.Collections.Generic;
using Search.BLL.Models;

namespace Search.BLL.Interfaces
{
    public interface ISearchService<out T>
    {
        IEnumerable<T> Search(SearchRequest query);
        IEnumerable<string> Autocomplete(string query);
    }
}