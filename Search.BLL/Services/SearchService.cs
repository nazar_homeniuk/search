﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Nest;
using Search.BLL.Interfaces;
using Search.Core.Models;
using Search.DAL.Config;
using Search.DAL.Models;
using SearchRequest = Search.BLL.Models.SearchRequest;

namespace Search.BLL.Services
{
    public class SearchService : ISearchService<ProductDto>
    {
        private readonly IElasticClient elasticClient;

        public SearchService()
        {
            elasticClient = ElasticConfig.GetClient();
        }

        public IEnumerable<ProductDto> Search(SearchRequest request)
        {
            var searchQuery = new SearchDescriptor<Product>();
            searchQuery.Query(q => q
                                       .Range(r => r
                                           .Field(f => f.Price)
                                           .LessThanOrEquals(request.MaxPrice)
                                           .GreaterThanOrEquals(request.MinPrice))
                                   && q
                                       .MatchPhrasePrefix(m => m
                                           .Field(f => f.Title)
                                           .Query(request.Query))
                                   && q
                                       .Range(r => r
                                           .Field(f => f.Count)
                                           .GreaterThanOrEquals(request.Count)));
            var result = elasticClient.Search<Product>(searchQuery);
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Product, ProductDto>()).CreateMapper();
            return mapper.Map<IEnumerable<Product>, List<ProductDto>>(result.Documents);
        }

        public IEnumerable<string> Autocomplete(string query)
        {
            var result = elasticClient.Search<Product>(s => s.Query(q => q
                .Prefix(p => p
                    .Name("title")
                    .Boost(1.1)
                    .Field(f => f.Title)
                    .Value(query)
                    .Rewrite(MultiTermQueryRewrite.TopTerms(10))))
            );

            return result.Documents.Select(t => t.Title);
        }
    }
}
