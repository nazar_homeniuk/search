﻿using System;
using System.IO;
using System.Threading.Tasks;
using Nest;
using Search.Core.Models;
using Search.DAL.Config;
using Search.DAL.Interfaces;
using Search.DAL.Models;
using Search.DAL.Providers;

namespace Search.BLL.Services
{
    public class IndexService
    {
        private readonly IElasticClient elasticClient;
        private readonly IXmlDataProvider<Product> xmlDataProvider;

        public IndexService()
        {
            elasticClient = ElasticConfig.GetClient();
            xmlDataProvider = new XmlDataProvider();
        }

        public async Task Index(object sender, FileSystemEventArgs eventArgs)
        {
            if (elasticClient.IndexExists(ElasticConfig.IndexName).Exists)
            {
                elasticClient.DeleteIndex(ElasticConfig.IndexName);
            }

            foreach (var product in xmlDataProvider.ReadFromDirectory(
                AppDomain.CurrentDomain.BaseDirectory + "/data/"))
            {
                await elasticClient.IndexDocumentAsync(product);
            }
        }

        public async Task AddProduct(ProductDto productDto)
        {
            var product = new Product
            {
                Id = Guid.NewGuid().ToString(),
                Category = productDto.Category,
                Count = productDto.Count,
                Price = productDto.Price,
                ShopAddress = productDto.ShopAddress,
                ShopName = productDto.ShopName,
                ShopCity = productDto.ShopCity,
                Tags = productDto.Tags,
                Title = productDto.Title,
                Suggest = new CompletionField {Input = (string[]) productDto.Tags.Clone()}
            };
            xmlDataProvider.Create(product, AppDomain.CurrentDomain.BaseDirectory + "/data/");
            await elasticClient.IndexDocumentAsync(product);
        }

        public async Task UpdateProduct(ProductDto productDto)
        {
            var product = new Product
            {
                Id = productDto.Id,
                Category = productDto.Category,
                Count = productDto.Count,
                Price = productDto.Price,
                ShopAddress = productDto.ShopAddress,
                ShopName = productDto.ShopName,
                ShopCity = productDto.ShopCity,
                Tags = productDto.Tags,
                Title = productDto.Title,
                Suggest = new CompletionField { Input = (string[])productDto.Tags.Clone() }
            };
            xmlDataProvider.Update(product, AppDomain.CurrentDomain.BaseDirectory + "/data/");
            await elasticClient.DeleteByQueryAsync<Product>(d => d.Query(q => q.Match(m => m.Field(f => f.Id == productDto.Id))));
            await elasticClient.IndexDocumentAsync(product);
        }
    }
}
