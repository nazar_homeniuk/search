﻿namespace Search.BLL.Models
{
    public class SearchRequest
    {
        public string Query { get; set; }
        public int? MinPrice { get; set; }
        public int? MaxPrice { get; set; }
        public int? Count { get; set; }
    }
}
