﻿namespace Search.Core.Interfaces.Query
{
    public interface IQueryDispatcher<out TResult>
    {
        TResult Execute<TQuery>(TQuery query) where TQuery : IQuery;
    }
}