﻿using Search.Core.Interfaces.Query;

namespace Search.Core.Interfaces
{
    public interface IQueryHandler<in TQuery, out TResult> where TQuery : IQuery
    {
        TResult Handle(TQuery query);
    }
}