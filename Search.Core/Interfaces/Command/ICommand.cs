﻿namespace Search.Core.Interfaces.Command
{
    public interface ICommand : NServiceBus.ICommand
    {
    }
}