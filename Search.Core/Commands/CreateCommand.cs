﻿using Search.Core.Interfaces.Command;
using Search.Core.Models;

namespace Search.Core.Commands
{
    public class CreateCommand : ICommand
    {
        public ProductDto Product { get; }

        public CreateCommand(ProductDto product)
        {
            this.Product = product;
        }
    }
}