﻿using System.Xml.Serialization;
using Nest;
using Newtonsoft.Json;

namespace Search.Core.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public class ProductDto
    {
        [JsonProperty]
        [Text(Name = "id")]
        public string Id { get; set; }
        [JsonProperty]
        [Text(Name = "title")]
        public string Title { get; set; }
        [JsonProperty]
        [Text(Name = "category")]
        public string Category { get; set; }
        [JsonProperty]
        [Text(Name = "shop_name")]
        public string ShopName { get; set; }
        [JsonProperty]
        [Text(Name = "shop_address")]
        public string ShopAddress { get; set; }
        [JsonProperty]
        [Text(Name = "shop_city")]
        public string ShopCity { get; set; }
        [JsonProperty]
        [Number]
        public decimal Price { get; set; }
        [JsonProperty]
        [Number]
        public int Count { get; set; }
        [JsonProperty]
        public string[] Tags { get; set; }
        [XmlIgnore]
        public CompletionField Suggest { get; set; }
    }
}
