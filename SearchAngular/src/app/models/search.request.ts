export class SearchRequest {
    query: string;
    minPrice: number;
    maxPrice: number;
    count: number;
}