export class Product {
    Id: string;
    Title: string;
    Category: string;
    ShopName: string;
    ShopAddress: string;
    ShopCity: string;
    Price: number;
    Count: number;
    Tags: string[];
}