import { Component, OnInit, Injectable, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatSnackBar, MatDialog } from '@angular/material';
import { FilterDialogComponent } from '../filter-dialog/filter-dialog.component';
import { Router } from '@angular/router';
import { SearchService } from 'src/app/services';
import { SearchRequest, Product } from 'src/app/models';
import { CreateDialogComponent } from '../create-dialog/create-dialog.component';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
})

@Injectable()
export class SearchComponent implements OnInit {

  searchReqest: SearchRequest;
  createProduct: Product;
  searchInput: FormControl;
  searchResult: Product[];
  options: string[];

  constructor(private searchService: SearchService, private snackBar: MatSnackBar, private router: Router, public dialog: MatDialog) {
  }

  ngOnInit() {
    this.searchReqest = new SearchRequest();
    this.searchInput = new FormControl();
  }

  search() {
    this.searchReqest.query = this.searchInput.value;
    this.searchService.search(this.searchReqest).subscribe(result => {
      if (result.length != 0) {
        this.searchService.foundProducts = result;
        this.router.navigate(["/dashboard"]);
      }
      else {
        this.openSnackBar("Nothing was found :(");
      }
    });
  }

  onSearchChange(searchValue: string) {
    if (searchValue.length != 0) {
      this.searchService.autocomplete(searchValue).subscribe(result => {
        this.options = result;
      });
    }
  }

  openFiltersDialog(): void {
    const dialogRef = this.dialog.open(FilterDialogComponent, {
      width: '250px',
      data: { request: this.searchReqest }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.searchReqest = result;
    });
  }

  openCreateDialog(): void {
    this.dialog.open(CreateDialogComponent, {
      width: '250px'
    });
  }

  private openSnackBar(message: string) {
    this.snackBar.open(message, 'Dismiss', {
      duration: 3000
    });
  }
}
