import { Component, OnInit, Input } from '@angular/core';
import { SearchService } from 'src/app/services';

@Component({
  selector: 'app-cards-dashboard',
  templateUrl: './cards-dashboard.component.html',
  styleUrls: ['./cards-dashboard.component.css']
})
export class CardsDashboardComponent implements OnInit {
  
  constructor(private searchService: SearchService) { }

  ngOnInit() {
  }

}
