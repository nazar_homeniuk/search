import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SearchComponent } from '..';
import { Product } from 'src/app/models';
import { ProductsService } from 'src/app/services';

@Component({
  selector: 'app-create-dialog',
  templateUrl: './create-dialog.component.html',
  styleUrls: ['./create-dialog.component.css']
})
export class CreateDialogComponent implements OnInit {

  product: Product;
  tags: string;

  constructor(private productsService: ProductsService, public dialogRef: MatDialogRef<SearchComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Product) { }

  ngOnInit() {
    this.product = new Product();
  }

  create() {
    this.product.Tags = this.tags.split(',');
    this.productsService.create(this.product);
  }
}
