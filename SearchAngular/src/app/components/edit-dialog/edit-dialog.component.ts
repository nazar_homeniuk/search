import { Component, OnInit, Inject } from '@angular/core';
import { Product } from 'src/app/models';
import { ProductsService } from 'src/app/services';
import { ProductCardComponent } from '../product-card/product-card.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-edit-dialog',
  templateUrl: './edit-dialog.component.html',
  styleUrls: ['./edit-dialog.component.css']
})
export class EditDialogComponent implements OnInit {

  product: Product;
  tags: string;

  constructor(private productsService: ProductsService, public dialogRef: MatDialogRef<ProductCardComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Product) { }

  ngOnInit() {
    this.product = this.data;
  }

  update() {
    this.product.Tags = this.tags.split(',');
    this.productsService.create(this.product);
  }

}
