import { Component, OnInit, Input } from '@angular/core';
import { Product } from 'src/app/models';
import { MatDialog } from '@angular/material';
import { EditDialogComponent } from '../edit-dialog/edit-dialog.component';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.css']
})
export class ProductCardComponent implements OnInit {

  @Input() card: Product;
  
  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

  openEditDialog() {
    this.dialog.open(EditDialogComponent, {
      width: '250px',
      data: { product: this.card }
    });
  }
}
