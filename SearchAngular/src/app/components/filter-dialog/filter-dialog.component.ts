import { Component, Inject, Injectable } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SearchComponent } from '../search/search.component';
import { SearchRequest } from 'src/app/models';

@Component({
  selector: 'app-filter-dialog',
  templateUrl: './filter-dialog.component.html',
  styleUrls: ['./filter-dialog.component.css']
})

@Injectable()
export class FilterDialogComponent {

  min = 0;
  max = 100;
  step = 1;

  constructor(
    public dialogRef: MatDialogRef<SearchComponent>,
    @Inject(MAT_DIALOG_DATA) public data: SearchRequest) {}
}
