import { Injectable } from '@angular/core';
import { Product } from '../models';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private httpClient: HttpClient) { }

  create(product: Product) { 
    this.httpClient.post<any>(`${environment.API_URL}product/create`, product).subscribe(response => {
    });
  }

  update(product: Product) {
    this.httpClient.post<any>(`${environment.API_URL}product/update`, product).subscribe(response => {
    });
  }
}
