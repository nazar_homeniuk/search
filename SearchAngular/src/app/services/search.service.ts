import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SearchRequest, Product } from '../models';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  foundProducts: Product[];
  autocompleteResults: string[];

  constructor(private httpClient: HttpClient) { }

  search(request: SearchRequest): Observable<Product[]> {
    return this.httpClient.post<Product[]>(`${environment.API_URL}search`, request);
  }

  autocomplete(value: string): Observable<string[]> {
    return this.httpClient.get<string[]>(`${environment.API_URL}search/autocomplete/` + value);
  }
}
