import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatPaginatorModule } from '@angular/material/paginator';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSliderModule } from '@angular/material/slider';
import { MatDividerModule } from '@angular/material/divider';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatBadgeModule } from '@angular/material/badge';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { SearchComponent, FilterDialogComponent, ProductCardComponent, CardsDashboardComponent, CreateDialogComponent, EditDialogComponent } from './components';
import { SearchService, ProductsService } from './services';

@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    FilterDialogComponent,
    ProductCardComponent,
    CardsDashboardComponent,
    CreateDialogComponent,
    EditDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatAutocompleteModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatSnackBarModule,
    MatPaginatorModule,
    FlexLayoutModule,
    MatDialogModule,
    ScrollToModule.forRoot(),
    MatSliderModule,
    MatDividerModule,
    MatGridListModule,
    MatBadgeModule,
    MatProgressSpinnerModule
  ],
  providers: [
    SearchService,
    ProductsService
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    FilterDialogComponent,
    CreateDialogComponent,
    EditDialogComponent
  ]
})
export class AppModule { }
